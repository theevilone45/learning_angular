import { Injectable } from '@angular/core';
import { Student } from '../student';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private studentsApiUrl = 'https://localhost:5001/api/students';

  constructor(private http_client: HttpClient) { }

  getStudents(): Observable<Student[]> {
    return this.http_client.get<Student[]>(this.studentsApiUrl);
  }

  getStudent(id: number): Observable<Student> {
    const url = `${this.studentsApiUrl}/${id}`;
    return this.http_client.get<Student>(url);
  }

  updateStudent(student: Student): Observable<any> {
    const url = `${this.studentsApiUrl}/${student.id}`;
    return this.http_client.put(url, student, httpOptions);
  }

  createStudent(student: Student): Observable<Student> {
    return this.http_client.post<Student>(this.studentsApiUrl, student, httpOptions);
  }

  deleteStudent(student: Student | number): Observable<Student> {
    const id = typeof student === 'number' ? student : student.id;
    const url = `${this.studentsApiUrl}/${id}`;
    return this.http_client.delete<Student>(url, httpOptions);
  }
}
