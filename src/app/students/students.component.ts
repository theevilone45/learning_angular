import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
import { Student } from '../student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students: Student[] = [];

  constructor(private studentService: StudentService) { }

  ngOnInit(): void {
    this.getStudents();
  }

  getStudents(): void {
    this.studentService.getStudents().subscribe(
      students => this.students = students
    );
  }

  create(index: number, first_name: string, last_name: string): void {
    this.studentService.createStudent({
      index: index,
      firstName: first_name,
      lastName: last_name
    } as Student ).subscribe(
      student => {this.students.push(student);}
    );
  } 

  delete(student: Student): void {
    this.students = this.students.filter(s => s.id !== student.id);
    this.studentService.deleteStudent(student).subscribe();
  }

}
